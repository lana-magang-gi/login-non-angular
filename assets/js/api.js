$(window).ready( () => {
  getAll();

  $('.btAdd').on('click', () => {
    $('#formAdd').trigger('reset');
    $('#modalAdd').modal();
  })
})

function getAll() {
  $.ajax({
    url: 'assets/data/santri.json',
    dataType: 'json',
    type: 'GET',
    success: result => {
        $.each( result.santri, (index, data) => {
          $('tbody').append(`
            <tr>
              <th scope="row"> ${index+1}</th>
              <td>${data.nama}</td>
              <td>${data.gender}</td>
              <td>${data.birth}</td>
              <td>${data.alamat}</td>
              <td>${data.status}</td>
              <td>
                  <button type="button" class="btEdit btn btn-success" onclick="editModal(${data.id})"> Edit </button> &nbsp;
                  <button type="button" class="btDelete btn btn-danger" onclick="deleteData(${data.id})"> Delete </button> 
              </td>
            </tr>
          `);
        });
    },
    error: err => {
        console.log(err)
    }
  });
}

function editModal(id) {
  $('#modalAdd').modal();
  getById(id);
}

function getById(id) {
  $.ajax({
    url: 'assets/data/santri.json',
    dataType: 'json',
    type: 'GET',
    success: result => {
      $.each(result.santri, (index, data) => {
        if(data.id === id) {
          $('input[name=name]').val(data.nama);
          $(`input[name=gender][value=${data.gender}]`).prop('checked', true);
          $('input[name=birth]').val(data.birth);
          $('input[name=address]').val(data.alamat);
          $('input[name=status]').val(data.status);
        }
      })
    },
    fail: err => {
      console.log(err);
    }
  })
}

function editData() {

}

function addData() {
  let data = {
    "nama": $('input[name=name]').val(),
    "gender": $('input[name=gender]:checked').val(),
    "birth": $('input[name=birth]').val(),
    "alamat": $('input[name=address]').val(),
    "status": $('input[name=status]').val()
  }

  $.ajax({
    url: 'assets/data/santri.json',
    dataType: 'json',
    success: result => {
      if (!result.santri.push(data)) {
        return;
      }

      $('tbody').html('');

      $.each(result.santri, (index, data) => {
        $('tbody').append(`
          <tr>
            <th scope="row"> ${index+1}</th>
            <td>${data.nama}</td>
            <td>${data.gender}</td>
            <td>${data.birth}</td>
            <td>${data.alamat}</td>
            <td>${data.status}</td>
            <td>
              <button type="button" class="btEdit btn btn-success" onclick="editModal(${data.id})"> Edit </button> &nbsp;
              <button type="button" class="btDelete btn btn-danger" onclick="deleteData(${data.id})"> Delete </button> 
            </td>
          </tr>
        `);
      });

      $('#modalAdd').modal('hide');
    }
  })
}


function deleteData(id) {
  $.ajax({
    url: 'assets/data/santri.json',
    dataType: 'json',
    success: result => {

      if (!result.santri.splice(id, 1)) {
        return;
      } 
      
      $('tbody').html('');

      $.each(result.santri, (index, data) => {
        $('tbody').append(`
          <tr>
            <th scope="row"> ${index+1}</th>
            <td>${data.nama}</td>
            <td>${data.gender}</td>
            <td>${data.birth}</td>
            <td>${data.alamat}</td>
            <td>${data.status}</td>
            <td>
              <button type="button" class="btEdit btn btn-success" onclick="editModal(${data.id})"> Edit </button> &nbsp;
              <button type="button" class="btDelete btn btn-danger" onclick="deleteData(${data.id})"> Delete </button> 
            </td>
          </tr>
        `);
      });
    }
  })
}